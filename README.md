# RideCo Map Web Interface

Map application to mark and share points of interest

> This work is in progress and could not be completed within allotted time.
> However, the backend API has been completed (except it hasn't been tested).

![](https://user-images.githubusercontent.com/20656657/131846534-499b7e5d-fbcb-4e09-8507-cb597c796480.png)

## Try it!

Visit [https://rideco.chumaumenze.com/shares/4e7d3d82f4d5464388f2db912b549787](https://rideco.chumaumenze.com/shares/4e7d3d82f4d5464388f2db912b549787)

##### How to create markers

- Click anywhere on the map to create a marker/POI (point of interest)
- Create as many markers as desired
- Click the bottom-left button to view all markers
- Click the `Share with...` button to create and share a marker collection
- Copy the link generated and share with anyone.

##### How to view shared markers

- Open a share link provided to you in a browser.
- All markers will be loaded
- You can add more markers to the collection and share with anyone.

## Features

- Signup/Login users
- Organise markers in collections
- Share collection of markers to public
- Share collection of markers to specific users

## Requirement

- Node v14.x
- Yarn

### Getting Started

Define the following in your `.env` file in the project root directory.

Execute the following:

```shell
yarn install
export VUE_APP_RIDE_API_URL=http://localhost:8000
yarn serve

```

### Links

The application consists of a frontend and a backend.

Application
- [Map app][frontend-web]
- [API Docs][backend-api]

Source code
- [API source code][backend-repo]
- [Frontend source code][frontend-repo]


[frontend-repo]: https://gitlab.com/chumaumenze/rideco-web
[frontend-web]: https://rideco.chumaumenze.com
[backend-repo]: https://gitlab.com/chumaumenze/rideco-api
[backend-api]: https://riderco-map.herokuapp.com/docs
