import * as L from "leaflet";
import {AxiosStatic} from "axios";

function addMarker(map: L.Map, latlng: L.LatLngTuple, pophtml: string) {
    let marker = L.marker(latlng).addTo(map).bindPopup(pophtml).openPopup()
    return marker
}

function loadMarkers(apiClient: AxiosStatic, collectionUID: string) {
    let markers;
    let errMessage = null;
    apiClient.get(`/public/${collectionUID}`).then((r:any) => {
        let data = r.data;
        markers = data.markers;
    }).catch((e: any) => {
        console.log("API Error", e)
        errMessage = 'Failed to load marker collection'
        let data = e.response.data
        if (data.message) {
            errMessage = `${errMessage}: ${data.message}`
        }
    })
    return [markers, errMessage]
}

export {
    addMarker, loadMarkers
}
