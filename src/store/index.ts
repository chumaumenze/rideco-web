import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const defaultUserData = {
  id: "",
  firstName: "",
  lastName: "",
  username: "",
  auth: {
    token: "",
    tokenExpiresIn: Date.now(),
  }
}

export default new Vuex.Store({
  state: {
    markers: [],
    ridecoCurrentUser: Object.assign({}, defaultUserData)
  },
  getters: {
    authToken(state) {
      let userData = state.ridecoCurrentUser
      if (!userData.auth.token && !(userData.auth.tokenExpiresIn > Date.now())) {
        userData = Object.assign({}, defaultUserData)
      }
      return userData.auth.token
    }
  },
  mutations: {
    setCurrentUser(state, userData){
      console.log(userData)
      state.ridecoCurrentUser = userData;
      if (typeof (userData.auth.tokenExpiresIn) !== 'string') {
        userData.auth.tokenExpiresIn = Date.parse(userData.auth.tokenExpiresIn)
      }
      localStorage.setItem('ridecoCurrentUserData', JSON.stringify(userData))
    },
    addMarker(state, markerData) {
      state.markers.push(<never>markerData)
    }
  },
  actions: {
  },
  modules: {
  }
})
