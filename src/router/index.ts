import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Quickie from "@/views/Quickie.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/:markersData?',
    alias: '/shares/:markersData?',
    name: 'Quickie',
    component: Quickie,
  },
  {
    path: '/map',
    // alias: '/map',
    name: 'Map',
    component: () => import('../views/MapView.vue'),
  },
  {
    path: '*',
    name: 'Errors',
    component: () => import("../views/Error.vue"),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
