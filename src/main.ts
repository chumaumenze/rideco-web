import Vue from 'vue'
import axios, { AxiosStatic } from 'axios';
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$axios = axios;
Vue.prototype.$api = axios.create({
  baseURL: process.env.VUE_APP_RIDE_API_URL,
});

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosStatic;
    $api: AxiosStatic;
  }
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
